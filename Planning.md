# Planning du projet : 

| Nom |Prénom|Tâche|Temps estimé|Temps réel|
|--|--|--|--|--|
| 22/09/2021 |  |  |  |  |
| MICHOT | Julien | Regroupement des scénarios  | 1h | 1h30 |
| KOZLOV | Antoine | Regroupement des scénarios | 1h | 1h30 |
| POCHET | Antoine | Regroupement des scénarios | 1h | 1h30 |
| EL IDRISSI | Elyas | Création CU | 2h | 1h |
| TOUS |  | Création du GIT et arborescence  | 1h | 1h |
| 29/09/2021 |  |  |  |  |
| MICHOT | Julien | Création diagrammes de classes | 3h | 3h |
| KOZLOV | Antoine | Création diagrammes de classes | 3h | 3h |
| POCHET | Antoine | Rédaction des scénarios concrets et arborescence du GIT | 2h | 2h |
| EL IDRISSI | Elyas | Rédaction des scénarios concrets et modification CU | 2h | 2h |
| 05/10/2021 |  |  |  |  |
| MICHOT | Julien | Création diagrammes de classes | 1h | 1h |
| KOZLOV | Antoine | Création diagrammes de classes | 1h | 1h |
| POCHET | Antoine | Rédaction du glossaire métier et arborescence du GIT | 1h | 1h |
| EL IDRISSI | Elyas | Création diagrammes de classes | 4h | 7h |

---
*[Accueil](https://gitlab.univ-lille.fr/idm-e-kip/bourse-aux-livres/-/blob/master/README.md)*