# Diagrammes

### [Diagramme de classe](https://gitlab.univ-lille.fr/idm-e-kip/bourse-aux-livres/-/blob/master/diagrammes/Global.PNG)
---
### [Diagramme Scénario 1 : Demande d’adhésion à l’association](https://gitlab.univ-lille.fr/idm-e-kip/bourse-aux-livres/-/blob/master/diagrammes/SC1.PNG)

### [Diagramme Scénario 2 : Retrait des livres et des achats groupé](https://gitlab.univ-lille.fr/idm-e-kip/bourse-aux-livres/-/blob/master/diagrammes/SC2.PNG)

### [Diagramme Scénario 3 : Retour des livres](https://gitlab.univ-lille.fr/idm-e-kip/bourse-aux-livres/-/blob/master/diagrammes/SC3.PNG)

### [Diagramme Scénario 4 : Voir la disponibilité des livres et des achats groupés](https://gitlab.univ-lille.fr/idm-e-kip/bourse-aux-livres/-/blob/master/diagrammes/SC4.PNG)

### [Diagramme Scénario 5 : Gérer les stocks et les demandes d'achat groupé](https://gitlab.univ-lille.fr/idm-e-kip/bourse-aux-livres/-/blob/master/diagrammes/SC5.PNG)

### [Diagramme Scénario 6 : Faire une demande d'achats groupés](https://gitlab.univ-lille.fr/idm-e-kip/bourse-aux-livres/-/blob/master/diagrammes/SC6.PNG)

### [Diagramme Scénario 7 : Supprimer un adhérant](https://gitlab.univ-lille.fr/idm-e-kip/bourse-aux-livres/-/blob/master/diagrammes/SC7.PNG)

---
*[Accueil](https://gitlab.univ-lille.fr/idm-e-kip/bourse-aux-livres/-/blob/master/README.md)*