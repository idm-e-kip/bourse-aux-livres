# Scénario 7 : Supprimer un adhérant

**Acteurs :**<br/>

Alice, la gestionnaire de la Bourse Aux Livres<br/>

**Description :** <br/>

C’est la fin de l’année, Alice souhaite supprimer les adhérents dont tous les livres ont été rendus et dont tous les achats groupés ont été récupérés.<br/>
Alice entre ces critères dans l’application et supprime l’ensemble de la sélection d’adhérent.<br/>

**Conditions :** <br/>

---
*[Scénario concrets](https://gitlab.univ-lille.fr/idm-e-kip/bourse-aux-livres/-/blob/master/scénarios_concrets/Readme.md)*