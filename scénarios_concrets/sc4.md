# Scénario 4 : Voir la disponibilité des livres et des achats groupés

**Acteurs :** 

Bob, le Lycéen<br/>
Alice, la gestionnaire de la Bourse Aux Livres<br/>

**Description :**<br/>

Bob souhaite savoir sa demande d'achat groupé de calculatrice a été accepté.<br/>
Bob se rend sur l'application et entre le nom de la calculatrice<br/>
et voit que la demande a été acceptée, que les calculatrice ont été commandées<br/>
et que sa calculatrice sera disponible dans 1 semaine.<br/>
Bob se rend ensuite au local pour savoir si son livre de mathématique est disponible.<br/>
Alice entre le nom du livre dans l'application et voit que le livre n'est pas encore disponible et l'indique à Bob.<br/>

**Conditions :**<br/>

---
*[Scénario concrets](https://gitlab.univ-lille.fr/idm-e-kip/bourse-aux-livres/-/blob/master/scénarios_concrets/Readme.md)*