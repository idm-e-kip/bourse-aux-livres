# Scénario 1 : Demande d’adhésion à l’association

**Acteurs :**<br/>

Bob, le Lycéen et Alice la gestionnaire de la Bourse Aux Livres<br/>

**Description :**<br/>

Bob se rend à la Bourse Aux Livres pour adhérer à l’association afin d’emprunter des livres.<br/>
Alice demande a Bob un certain nombre d’informations (à définir (nom, prénom, cursus, établissement…))<br/>
et vérifie que des conditions sont remplies pour valider l’adhésion. <br/>

**Conditions :** Aucunes <br/>

---
*[Scénario concrets](https://gitlab.univ-lille.fr/idm-e-kip/bourse-aux-livres/-/blob/master/scénarios_concrets/Readme.md)*