# Scénario 5 : Gérer les stocks et les demandes d'achat groupé

**Acteurs :**<br/>

Alice, la gestionnaire de la Bourse Aux Livres <br/>

**Description :** <br/>

Alice souhaite visualiser les stocks de livre de mathématique de première.<br/>
Alice entre le nom du livre dans l’application et se rends compte qu’il n’y en a plus assez et donc en commande 5.<br/>
Alice souhaite également visualiser les demandes d’achats groupés de calculatrice.<br/>
Alice entre dans l’application la référence de la calculatrice et voit que 10 adhérant on fait une demande d’achat groupé pour cette dernière.<br/>
Alice décide d'effectuer la commande car elle estime qu'il y a assez de demande.<br/>

**Conditions :**<br/>

---
*[Scénario concrets](https://gitlab.univ-lille.fr/idm-e-kip/bourse-aux-livres/-/blob/master/scénarios_concrets/Readme.md)*