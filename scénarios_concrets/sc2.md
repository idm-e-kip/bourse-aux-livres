# Scénario 2 : Retrait des livres et des achats groupés

**Acteurs :**<br/>

Bob, le Lycéen<br/>
Alice, la gestionnaire de la Bourse Aux Livres<br/>

**Description :**<br/>

Bob a commandé des livres et un achat groupé de calculatrice.<br/>
Bob souhaite récupérer ses commandes et se rend donc au local.<br/>
Alice l’accueil, et lui demande son nom et sa classe.<br/>
Elle vérifie sur l’application que Bob a bien payé ses commandes de livre. Bob ayant payé ses livres,<br/>
Alice lui demande de payer la calculatrice. Bob paye et récupère ses livres et sa calculatrice.<br/>
Alice enregistre dans l’application qu’elle a remis à Bob ses livres commandés ainsi que la calculatrice.<br/>

**Conditions :**<br/>

Bob doit avoir faire une commande au niveau de l'association pour faire un achat groupé et avoir fait une demande d'emprunt de livre.<br/>

---
*[Scénario concrets](https://gitlab.univ-lille.fr/idm-e-kip/bourse-aux-livres/-/blob/master/scénarios_concrets/Readme.md)*