# Scénario 3 : Retour des livres 

**Acteurs :**<br/>

Bob, le Lycéen<br/>
Alice, la gestionnaire de la Bourse Aux Livres<br/>

**Description :**<br/>

C’est la fin de l’année, Bob va rendre ses livres. Il se rend au local.<br/>
Alice l’accueil, et lui demande son nom et sa classe.<br/> 
Elle recherche sa fiche sur l’application, puis fait l’inventaire de ses livres.<br/>
Elle indique pour chaque livre s’il a été rendu.<br/>
L’application indique que Bob a rendu tous ces livres, et que la caution peut lui être rendu.<br/>
Alice rend la caution et l’indique dans l’application.<br/>

**Conditions :**<br/>

Bob doit être adhérent et avoir emprunté des livres<br/>

---
*[Scénario concrets](https://gitlab.univ-lille.fr/idm-e-kip/bourse-aux-livres/-/blob/master/scénarios_concrets/Readme.md)*