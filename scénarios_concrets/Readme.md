# Scénario concret

### [Scénario 1 : Demande d’adhésion à l’association](https://gitlab.univ-lille.fr/idm-e-kip/bourse-aux-livres/-/blob/master/scénarios_concrets/sc1.md)

### [Scénario 2 : Retrait des livres et des achats groupés](https://gitlab.univ-lille.fr/idm-e-kip/bourse-aux-livres/-/blob/master/scénarios_concrets/sc2.md)

### [Scénario 3 : Retour des livres](https://gitlab.univ-lille.fr/idm-e-kip/bourse-aux-livres/-/blob/master/scénarios_concrets/sc3.md)

### [Scénario 4 : Voir la disponibilité des livres et des achats groupés](https://gitlab.univ-lille.fr/idm-e-kip/bourse-aux-livres/-/blob/master/scénarios_concrets/sc4.md)

### [Scénario 5 : Gérer les stocks et les demandes d'achat groupé](https://gitlab.univ-lille.fr/idm-e-kip/bourse-aux-livres/-/blob/master/scénarios_concrets/sc5.md)

### [Scénario 6 : Faire une demande d'achats groupés](https://gitlab.univ-lille.fr/idm-e-kip/bourse-aux-livres/-/blob/master/scénarios_concrets/sc6.md)

### [Scénario 7 : Supprimer un adhérant](https://gitlab.univ-lille.fr/idm-e-kip/bourse-aux-livres/-/blob/master/scénarios_concrets/sc7.md)

---
*[Accueil](https://gitlab.univ-lille.fr/idm-e-kip/bourse-aux-livres/-/blob/master/README.md)*