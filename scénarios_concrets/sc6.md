# Scénario 6 : Faire une demande d'achats groupés 

**Acteurs :** 

Bob, le Lycéen<br/>
Alice, la gestionnaire de la Bourse Aux Livres<br/>

**Description :**<br/>

C’est le début de l’année, Bob souhaite effectuer un achat groupé pour un produit.<br/>
Il se rend au local. Alice l’accueil. Bob indique à Alice qu’il souhaite effectuer un achat groupé.<br/> 
Alice lui demande son nom et sa classe et le produit souhaiter.<br/>
Alice enregistre dans l’application la demande d’achat groupé.<br/>

**Conditions :** <br/>

---
*[Scénario concrets](https://gitlab.univ-lille.fr/idm-e-kip/bourse-aux-livres/-/blob/master/scénarios_concrets/Readme.md)*