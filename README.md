## La bourse aux livres
### *Nom de l'équipe :*  e-kip
### *Date :* 05/10/2021
### *Auteurs :*
 - MICHOT Julien
 - KOZLOV Antoine
 - EL IDRISSI Elyas
 - POCHET Antoine
 
### *Version :* 1.0
---
### Objectif
 - Mettre en place un logiciel de gestion de bourse aux livres 

### Problème
 - Le problème est de gérer les adhésions, les livres distribués et les paiements plus simplement, tout en ayant une vision globale, simplifiée et informatisée pour faire gagner du temps aux gestionnaires de la bourse aux livres. 

### Résumé
 - Le but est d’informatiser la gestion de la BAL : gérer les adhésions, gérer les livres distribués, gérer les paiements.Toutes les procédures de gestion sont déjà bien établies, et sont partiellement informatisées (mais sous la forme de feuille excel)

### Contexte
 - Dans les lycées, les livres de classe sont à la charge des familles. Autrement dit, c’est aux familles d’acheter les livres de
classe. Afin de diminuer le coût des livres, certaines associations de parents d’élèves de lycées organisent des ”Bourses
Aux Livres” (BAL) : les lycéens peuvent louer les livres de l’année pour une somme bien moindre que le prix de
l’achat des livres. Les associations peuvent aussi faire des achats groupés afin d’obtenir des prix sur par exemple les
calculatrices. Pour avoir les livres, les lycéens adhèrent à l’association. Celle-ci doit gérer les adhésions, la liste des livres distribués
et les paiements.


### Liens

 - [Scénarios concrets](https://gitlab.univ-lille.fr/idm-e-kip/bourse-aux-livres/-/blob/master/scénarios_concrets/Readme.md)
 - [Cas d'utilisation](https://gitlab.univ-lille.fr/idm-e-kip/bourse-aux-livres/-/blob/master/cas_utilisation/Readme.md "")
 - [Diagrammes](https://gitlab.univ-lille.fr/idm-e-kip/bourse-aux-livres/-/blob/master/diagrammes/Readme.md "")
 - [Glossaire métier](https://gitlab.univ-lille.fr/idm-e-kip/bourse-aux-livres/-/blob/master/Glossaire_métier.md "")
 - [Planning](https://gitlab.univ-lille.fr/idm-e-kip/bourse-aux-livres/-/blob/master/Planning.md "")