### Adhérant
Un adhérant représente une personne physique inscrite dans l'école. Il va donc appartenir à un niveau et il aura des matières. 
<br/> <br/>
### Application
L'application représente l'outil dématérialisé permettant le suivi et la gestion globale de la BAL.
<br/> <br/>
### Article(s)
Un article est un objet mis en location ou en vente pour les adhérents.
<br/> <br/>
### Gestionnaire
Un gestionnaire est une personne travaillant au BAL, gère l'application, le stock, les adhérents.
<br/> <br/>
### Local
Un local représente le lieu de travail et de stockage du BAL.
<br/> <br/>
### Niveau 
Niveau représente un niveau de classe. Par exemple, pour un lycée, on a les niveaux suivants : seconde, première et
terminale.
<br/> <br/>
### Matière
Matière est un enseignement suivie par un élève. Une matière a un nom (Math, français, Anglais ...). Il y a des
matières optionnelles, des langues, des matières obligatoire et des matières de spécialité. Une matière est attaché à un
niveau. La matière Français de seconde est différente de la matière Français de première.

---
*[Accueil](https://gitlab.univ-lille.fr/idm-e-kip/bourse-aux-livres/-/blob/master/README.md)*