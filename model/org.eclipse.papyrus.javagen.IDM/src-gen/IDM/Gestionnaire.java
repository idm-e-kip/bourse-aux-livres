// --------------------------------------------------------
// Code generated by Papyrus Java
// --------------------------------------------------------

package IDM;

/************************************************************/
/**
 * 
 */
public class Gestionnaire extends Utilisateur {
	/**
	 * 
	 */
	public Mat�riel[] mat�riel;
	/**
	 * 
	 */
	public Livre[] livre;
	/**
	 * 
	 */
	public Location[] locations;

	/**
	 * 
	 * @param application 
	 * @param nouveauAdh�rant 
	 */
	public void ajouterAdh�rant(Application application, Adh�rant nouveauAdh�rant) {
	}

	/**
	 * 
	 * @param adh�rant 
	 * @return 
	 */
	public Mat�riel[] getMat�riauxAchet�s(Adh�rant adh�rant) {
	}

	/**
	 * 
	 * @param adh�rant 
	 * @return 
	 */
	public Location[] getLocations(Adh�rant adh�rant) {
	}

	/**
	 * 
	 * @param mat�riel 
	 */
	public void setMat�rielDonn�(Mat�riel mat�riel) {
	}

	/**
	 * 
	 * @param achat 
	 */
	public void setLocationDonn�(Location achat) {
	}

	/**
	 * 
	 * @param location 
	 */
	public void setLocationR�cup�r�(Location location) {
	}

	/**
	 * 
	 * @param app 
	 */
	public void getLivres(Application app) {
	}

	/**
	 * 
	 * @param app 
	 * @param livre 
	 */
	public void commanderLivre(Application app, Livre livre) {
	}

	/**
	 * 
	 * @param demandeAchatGroup� 
	 */
	public void accepterDemandeAchatGroup�(DemandeAchatGroup� demandeAchatGroup�) {
	}

	/**
	 * 
	 * @param app 
	 * @param demandeAchatGroup� 
	 */
	public void commanderAchatGroup�(Application app, DemandeAchatGroup� demandeAchatGroup�) {
	}

	/**
	 * 
	 * @param app 
	 */
	public void getDemandesAchatGroup�(Application app) {
	}

	/**
	 * 
	 * @param app 
	 * @param nomMat�riel 
	 * @param adh�rant 
	 */
	public void ajouterDemandeAchatGroup�(Application app, String nomMat�riel, Adh�rant adh�rant) {
	}

	/**
	 * 
	 * @param app 
	 * @param adh�rant 
	 */
	public void supprimerAdh�rant(Application app, Adh�rant adh�rant) {
	}
}
